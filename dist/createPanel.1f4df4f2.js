// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"assets/result.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var result = [{
  "Flat UI Palette v1": ["rgb(26,188,156)", "rgb(46,204,113)", "rgb(52,152,219)", "rgb(155,89,182)", "rgb(52,73,94)", "rgb(22,160,133)", "rgb(39,174,96)", "rgb(41,128,185)", "rgb(142,68,173)", "rgb(44,62,80)", "rgb(241,196,15)", "rgb(230,126,34)", "rgb(231,76,60)", "rgb(236,240,241)", "rgb(149,165,166)", "rgb(243,156,18)", "rgb(211,84,0)", "rgb(192,57,43)", "rgb(189,195,199)", "rgb(127,140,141)"]
}, {
  "American Palette": ["rgb(85,239,196)", "rgb(129,236,236)", "rgb(116,185,255)", "rgb(162,155,254)", "rgb(223,230,233)", "rgb(0,184,148)", "rgb(0,206,201)", "rgb(9,132,227)", "rgb(108,92,231)", "rgb(178,190,195)", "rgb(255,234,167)", "rgb(250,177,160)", "rgb(255,118,117)", "rgb(253,121,168)", "rgb(99,110,114)", "rgb(253,203,110)", "rgb(225,112,85)", "rgb(214,48,49)", "rgb(232,67,147)", "rgb(45,52,54)"]
}, {
  "Aussie Palette": ["rgb(246,229,141)", "rgb(255,190,118)", "rgb(255,121,121)", "rgb(186,220,88)", "rgb(223,249,251)", "rgb(249,202,36)", "rgb(240,147,43)", "rgb(235,77,75)", "rgb(106,176,76)", "rgb(199,236,238)", "rgb(126,214,223)", "rgb(224,86,253)", "rgb(104,109,224)", "rgb(48,51,107)", "rgb(149,175,192)", "rgb(34,166,179)", "rgb(190,46,221)", "rgb(72,52,212)", "rgb(19,15,64)", "rgb(83,92,104)"]
}, {
  "British Palette": ["rgb(0,168,255)", "rgb(156,136,255)", "rgb(251,197,49)", "rgb(76,209,55)", "rgb(72,126,176)", "rgb(0,151,230)", "rgb(140,122,230)", "rgb(225,177,44)", "rgb(68,189,50)", "rgb(64,115,158)", "rgb(232,65,24)", "rgb(245,246,250)", "rgb(127,143,166)", "rgb(39,60,117)", "rgb(53,59,72)", "rgb(194,54,22)", "rgb(220,221,225)", "rgb(113,128,147)", "rgb(25,42,86)", "rgb(47,54,64)"]
}, {
  "Canadian Palette": ["rgb(255,159,243)", "rgb(254,202,87)", "rgb(255,107,107)", "rgb(72,219,251)", "rgb(29,209,161)", "rgb(243,104,224)", "rgb(255,159,67)", "rgb(238,82,83)", "rgb(10,189,227)", "rgb(16,172,132)", "rgb(0,210,211)", "rgb(84,160,255)", "rgb(95,39,205)", "rgb(200,214,229)", "rgb(87,101,116)", "rgb(1,163,164)", "rgb(46,134,222)", "rgb(52,31,151)", "rgb(131,149,167)", "rgb(34,47,62)"]
}, {
  "Chinese Palette": ["rgb(236,204,104)", "rgb(255,127,80)", "rgb(255,107,129)", "rgb(164,176,190)", "rgb(87,96,111)", "rgb(255,165,2)", "rgb(255,99,72)", "rgb(255,71,87)", "rgb(116,125,140)", "rgb(47,53,66)", "rgb(123,237,159)", "rgb(112,161,255)", "rgb(83,82,237)", "rgb(255,255,255)", "rgb(223,228,234)", "rgb(46,213,115)", "rgb(30,144,255)", "rgb(55,66,250)", "rgb(241,242,246)", "rgb(206,214,224)"]
}, {
  "Dutch Palette": ["rgb(255,195,18)", "rgb(196,229,56)", "rgb(18,203,196)", "rgb(253,167,223)", "rgb(237,76,103)", "rgb(247,159,31)", "rgb(163,203,56)", "rgb(18,137,167)", "rgb(217,128,250)", "rgb(181,52,113)", "rgb(238,90,36)", "rgb(0,148,50)", "rgb(6,82,221)", "rgb(153,128,250)", "rgb(131,52,113)", "rgb(234,32,39)", "rgb(0,98,102)", "rgb(27,20,100)", "rgb(87,88,187)", "rgb(111,30,81)"]
}, {
  "French Palette": ["rgb(250,211,144)", "rgb(248,194,145)", "rgb(106,137,204)", "rgb(130,204,221)", "rgb(184,233,148)", "rgb(246,185,59)", "rgb(229,80,57)", "rgb(74,105,189)", "rgb(96,163,188)", "rgb(120,224,143)", "rgb(250,152,58)", "rgb(235,47,6)", "rgb(30,55,153)", "rgb(60,99,130)", "rgb(56,173,169)", "rgb(229,142,38)", "rgb(183,21,64)", "rgb(12,36,97)", "rgb(10,61,98)", "rgb(7,153,146)"]
}, {
  "German Palette": ["rgb(252,92,101)", "rgb(253,150,68)", "rgb(254,211,48)", "rgb(38,222,129)", "rgb(43,203,186)", "rgb(235,59,90)", "rgb(250,130,49)", "rgb(247,183,49)", "rgb(32,191,107)", "rgb(15,185,177)", "rgb(69,170,242)", "rgb(75,123,236)", "rgb(165,94,234)", "rgb(209,216,224)", "rgb(119,140,163)", "rgb(45,152,218)", "rgb(56,103,214)", "rgb(136,84,208)", "rgb(165,177,194)", "rgb(75,101,132)"]
}, {
  "Indian Palette": ["rgb(254,164,127)", "rgb(37,204,247)", "rgb(234,181,67)", "rgb(85,230,193)", "rgb(202,211,200)", "rgb(249,127,81)", "rgb(27,156,252)", "rgb(248,239,186)", "rgb(88,177,159)", "rgb(44,58,71)", "rgb(179,55,113)", "rgb(59,59,152)", "rgb(253,114,114)", "rgb(154,236,219)", "rgb(214,162,232)", "rgb(109,33,79)", "rgb(24,44,97)", "rgb(252,66,123)", "rgb(189,197,129)", "rgb(130,88,159)"]
}, {
  "Russian Palette": ["rgb(243,166,131)", "rgb(247,215,148)", "rgb(119,139,235)", "rgb(231,127,103)", "rgb(207,106,135)", "rgb(241,144,102)", "rgb(245,205,121)", "rgb(84,109,229)", "rgb(225,95,65)", "rgb(196,69,105)", "rgb(120,111,166)", "rgb(248,165,194)", "rgb(99,205,218)", "rgb(234,134,133)", "rgb(89,98,117)", "rgb(87,75,144)", "rgb(247,143,179)", "rgb(61,193,211)", "rgb(230,103,103)", "rgb(48,57,82)"]
}, {
  "Spanish Palette": ["rgb(64,64,122)", "rgb(112,111,211)", "rgb(247,241,227)", "rgb(52,172,224)", "rgb(51,217,178)", "rgb(44,44,84)", "rgb(71,71,135)", "rgb(170,166,157)", "rgb(34,112,147)", "rgb(33,140,116)", "rgb(255,82,82)", "rgb(255,121,63)", "rgb(209,204,192)", "rgb(255,177,66)", "rgb(255,218,121)", "rgb(179,57,57)", "rgb(205,97,51)", "rgb(132,129,122)", "rgb(204,142,53)", "rgb(204,174,98)"]
}, {
  "Swedish Palette": ["rgb(239,87,119)", "rgb(87,95,207)", "rgb(75,207,250)", "rgb(52,231,228)", "rgb(11,232,129)", "rgb(245,59,87)", "rgb(60,64,198)", "rgb(15,188,249)", "rgb(0,216,214)", "rgb(5,196,107)", "rgb(255,192,72)", "rgb(255,221,89)", "rgb(255,94,87)", "rgb(210,218,226)", "rgb(72,84,96)", "rgb(255,168,1)", "rgb(255,211,42)", "rgb(255,63,52)", "rgb(128,142,155)", "rgb(30,39,46)"]
}, {
  "Turkish Palette": ["rgb(205,132,241)", "rgb(255,204,204)", "rgb(255,77,77)", "rgb(255,175,64)", "rgb(255,250,101)", "rgb(197,108,240)", "rgb(255,184,184)", "rgb(255,56,56)", "rgb(255,159,26)", "rgb(255,242,0)", "rgb(50,255,126)", "rgb(126,255,245)", "rgb(24,220,255)", "rgb(125,95,255)", "rgb(75,75,75)", "rgb(58,227,116)", "rgb(103,230,220)", "rgb(23,192,235)", "rgb(113,88,226)", "rgb(61,61,61)"]
}];
var _default = result;
exports.default = _default;
},{}],"src/utils/getPanelName.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getName;

// 返回一个数组 ，包含了每个panel的标题
function getName(result) {
  var names = result.map(function (panelObj, idx) {
    var currentName = Object.keys(panelObj)[0];
    return currentName;
  });
  return names;
}
},{}],"src/utils/OneDToND.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = oneToN;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function oneToN(row, col, arr) {
  // 一维数组 这么复制就算深复制了
  var copyArr = _toConsumableArray(arr); // 几行几列？ row行 col列


  var len = copyArr.length;

  if (len !== row * col) {
    throw new Error("做不了，个数不对");
  }

  var tempArr = [];

  for (var i = 0; i < row; i++) {
    //
    if (!tempArr[i]) {
      tempArr[i] = [];
    }

    var j = 0;

    while (j < col) {
      tempArr[i].push(copyArr.shift());
      j++;
    }
  } //   console.log(tempArr);


  return tempArr;
}
},{}],"src/createPanel.js":[function(require,module,exports) {
"use strict";

var _result = _interopRequireDefault(require("../assets/result"));

var _getPanelName = _interopRequireDefault(require("./utils/getPanelName"));

var _OneDToND = _interopRequireDefault(require("./utils/OneDToND"));

var _this = void 0;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

//ease function
function ease(t, b, c, d) {
  t /= d / 2;
  if (t < 1) return c / 2 * t * t * t * t + b;
  t -= 2;
  return -c / 2 * (t * t * t * t - 2) + b;
} //  2个页面


var container = document.querySelector(".container");
var underlying = document.querySelector(".underlying"); //  增加bigColorPanel 点击返回的功能
// 类名 (back-to-future)

var backToFuture = document.querySelector(".back-to-future");
backToFuture.addEventListener("click", function () {
  underlying.classList.remove("show");
  container.classList.remove("hide");
  var tmp = underlying.querySelector(".big-container");
  tmp.remove();
  document.querySelector("body").classList.remove("hide");
}); // ! 因为只有14个色板，所以索引是0-13 ;

var currentPanelIndex = {
  idx: 0,

  set currentIdx(num) {
    if (num < 0 || num > 13) {
      console.log("无法修改，超过索引");
      return;
    }

    this.idx = num; // console.log("修改成功", this);

    return this.idx;
  },

  get currentIdx() {
    return this.idx;
  },

  liveIdx: function liveIdx() {
    return _this.idx;
  }
}; //  原始的flat ui 上 一共十四个色盘 这里创建14个色盘
//  其实更好的 办法是固定写好在HTML里 ，但是我怕以后要增加色盘个数，所以写个能根据要求生成色盘的函数
// 创建14个 下面的结构

/**
    <div class="panel">
          <div class="colors">
            <div class="color"></div> 要20个
          </div>
          <div class="caption">
          </div>
        </div>
  */
//  配置就是 几个设定好的类名 ，和CSS 样式耦合 ，要改一起改。

var config = {
  panelClassName: "panel",
  colorsClassName: "colors",
  colorClassName: "color",
  captionClassName: "captions"
}; // 这是个辅助函数 把result里的的 name 以数组形式返回

var names = (0, _getPanelName.default)(_result.default); // 接受类名作为参数

function createDiv(className) {
  var div = document.createElement("div");
  div.classList.add(className);
  return div;
} // 接受参数 name 作为这个panel的标题 。
// 接受参数 idxOfResult, 选择result中第idx个对象
// 返回1个 div.panel 这个元素 有一个div.color ;div.color内部有2个div.colors
// 还有一个div.caption


function createPanel(name, idxOfResult) {
  var panelClassName = config.panelClassName,
      colorsClassName = config.colorsClassName,
      colorClassName = config.colorClassName,
      captionClassName = config.captionClassName;
  var currentPanel = createDiv(panelClassName); // !给 div.panel绑定事件监听处理函数

  currentPanel.addEventListener("mousedown", function (e) {
    // 让 色盘隐藏 ；这里body是让他背景色变成白色，没有也行
    setTimeout(function () {
      container.classList.add("hide");
      document.querySelector("body").classList.add("hide");
      document.querySelector("div.mr-white").classList.add("animateRight"); // window.requestAnimationFrame(moveLeft);
    }, 300); // 点击出现 一个遮罩左移的动画

    underlying.classList.add("show");
    var start = null;
    var element = document.getElementsByClassName("mask")[0]; //

    function moveLeft(timestamp) {
      if (!start) {
        start = timestamp;
        0;
        element.classList.add("show");
      }

      var progress = timestamp - start; // element.style.right = Math.min(progress / 10, 200) + 'px';
      // const run = ease(progress, sPostion, tPosition as number, duration);

      var left = ease(progress, 200, -200, 1000);
      var width = ease(progress, 200, -200, 1000);
      element.style.left = left + "px";
      element.style.width = width + "vw";

      if (progress < 1000) {
        window.requestAnimationFrame(moveLeft);
      }
    } // ! 使用全局变量 currentPanelIndex={idx:0} 默认idx是0


    currentPanelIndex.currentIdx = idxOfResult;
    var panelBeingClicked = e.currentTarget;
    var underlyingEle = new CreateBigPanel(idxOfResult); //  这是一个点击缩小又还原的动画

    panelBeingClicked.classList.add("panel-sm");
    setTimeout(function () {
      panelBeingClicked.classList.remove("panel-sm");
    }, 120); // console.log(currentPanelIndex);
  });
  var colors = createDiv(colorsClassName); // 选取result里第idx个 对象
  // console.log(idxOfResult);

  var currentObjOfResult = _result.default[idxOfResult]; // 这个对象对应的数组

  var currentKey = Object.keys(currentObjOfResult);
  var constRGBArr = currentObjOfResult[currentKey]; // 一个色块colors中有2个color

  var colorsNodeList = Array(20).fill("emptyString").map(function (itemThatIWontUse, idx) {
    var currentColor = createDiv(colorClassName);
    currentColor.style.backgroundColor = constRGBArr[idx];
    return currentColor;
  });
  Array(20).fill("why dont u use loop?dick head!").forEach(function (itemThatIWontUse, idx) {
    colors.appendChild(colorsNodeList[idx]);
  }); // 用panel组织 colors>.color*20+caption这个结构

  currentPanel.appendChild(colors); // caption 名字在这里添加吧

  var currentCaption = createDiv(captionClassName);
  currentCaption.innerHTML = name;
  currentPanel.appendChild(currentCaption); // 测试通过了
  // console.log(currentPanel);

  return currentPanel;
} // 测试通过了
// console.log(createPanel());
// 创建14个div.panel


var panels = Array(14).fill("i shoud use loop").map(function (placeholder, idx) {
  return createPanel(names[idx], idx);
}); // 测试通过了
// console.log(panels);
// const container = document.querySelector(".container");

Array(14).fill("i shoud use loop").forEach(function (item, idx) {
  container.appendChild(panels[idx]);
}); // 第二个页面

var configOfBigPanel = {
  bigContainer: "big-container",
  bigCaption: "big-caption",
  bigColors: "big-colors",
  row: "big-row",
  bigColor: "big-color"
};

var CreateBigPanel = /*#__PURE__*/function () {
  // 构造函数 创建 一个div.big-container
  // 创建一个div.big-caption
  // 创建一个div.big-colors
  // 创建20个div.big-color

  /**
       结构:
       div.big-container
          -div.big-caption
          -div.big-colors
              -div.bing-color*20
       */
  function CreateBigPanel(idx) {
    _classCallCheck(this, CreateBigPanel);

    this.currentIdx = idx;
    this.bigContainer = CreateBigPanel.createDiv(configOfBigPanel.bigContainer);
    this.bigCaption = CreateBigPanel.createDiv(configOfBigPanel.bigCaption);
    this.bigColors = CreateBigPanel.createDiv(configOfBigPanel.bigColors); // 是一个数组 五行 div.big-row

    this.bigRow = CreateBigPanel.createDivs(configOfBigPanel.row, 5); // 是一个数组 ，不要了
    // this.bigColorArr = CreateBigPanel.createDivs(
    //   configOfBigPanel.bigColors,
    //   20
    // );

    this.flashGone = document.querySelector(".flash-gone"); // 组合 辅助函数

    this._combine(); // 渲染到页面上


    document.querySelector(".underlying").appendChild(this.bigContainer);
  } // 辅助函数 创建div.className


  _createClass(CreateBigPanel, [{
    key: "_combine",
    value: // 组合 实例属性 形成要求的结构
    function _combine() {
      var _this2 = this;

      var currentPanelObj = _result.default[this.currentIdx];
      var currentPanelName = Object.keys(currentPanelObj)[0];
      var currentPanelArr = currentPanelObj[currentPanelName]; //! console.log(currentPanelArr); 测试通过 这是 当前要用到的色块的数组
      // 把5个小色块加到4行big-row中 一行+五个，所以一共20个
      // ! 把 1D数组 result 变成4*5数组  数组在这变了 ！ 要改在这改 （比如6行8列）

      var transformedArr = (0, _OneDToND.default)(4, 5, currentPanelArr);
      console.log(transformedArr);
      var bigRows = this.bigRow.map(function (row, ithRow) {
        // 一行+五个
        for (var i = 0; i < 5; i++) {
          if (ithRow < 4) {
            var currentColor = transformedArr[ithRow][i];
            var currentDiv = CreateBigPanel.createDiv(configOfBigPanel.bigColor);
            var copyButton = CreateBigPanel.createDiv("hover-show");
            copyButton.innerText = "COPY";
            copyButton.addEventListener("mousemove", function (e) {
              e.currentTarget.classList.add("show");
            });
            copyButton.addEventListener("mouseleave", function (e) {
              e.currentTarget.classList.remove("show");
            });
            currentDiv.append(copyButton);
            currentDiv.style.backgroundColor = currentColor; // currentDiv.innerHTML=currentColor

            currentDiv.addEventListener("click", function (e) {
              var colorToCopy = e.currentTarget.style.backgroundColor; //!复制粘贴操作!

              CreateBigPanel.createInputForCopy(colorToCopy); // 显示COPY的动画

              _this2.flashGone.classList.add("show");

              _this2.flashGone.style.backgroundColor = colorToCopy;
              setTimeout(function () {
                _this2.flashGone.classList.remove("show");
              }, 300);
            }); //  把 big-color小色块加到每一行里边

            row.append(currentDiv);
          }
        }

        return row;
      }); // 把4行 big-row加到 big-colors

      Array(4).fill("_").forEach(function (_, idx) {
        _this2.bigColors.append(bigRows[idx]);
      }); // this.bigColors.append(bigRows);
      // console.log(bigRows);

      this.bigContainer.append(this.bigCaption);
      this.bigContainer.append(this.bigColors); // 找到当前页面的 20个 big-color 色块
      // const underlying= document.querySelectorAll(".big-color");
      //   console.log(underlying);
      // return this._combine
    } //  插入到页面.mask中
    // 给这个对象增加返回

  }], [{
    key: "createDiv",
    value: function createDiv(className) {
      var div = document.createElement("div");
      div.classList.add(className);
      return div;
    } // 辅助函数 创建nums个div.className 返回一个数组

  }, {
    key: "createDivs",
    value: function createDivs(className, nums) {
      var i = 0;
      var divArr = [];

      while (i < nums) {
        var div = document.createElement("div");
        div.classList.add(className);
        divArr.push(div);
        i++;
      }

      return divArr;
    }
    /**
    https://stackoverflow.com/questions/33855641/copy-output-of-a-javascript-variable-to-the-clipboard
    copied from a post talking about copy!
    _(:з」∠)_
     */

  }, {
    key: "createInputForCopy",
    value: function createInputForCopy(val) {
      var dummy = document.createElement("input"); // !chrome  兼容问题？ 这一行不被采纳?
      // dummy.style.display = "none";

      document.body.appendChild(dummy);
      dummy.setAttribute("id", "dummy_id");
      document.getElementById("dummy_id").value = val;
      dummy.select();
      document.execCommand("copy");
      document.body.removeChild(dummy);
    }
  }]);

  return CreateBigPanel;
}(); //
},{"../assets/result":"assets/result.js","./utils/getPanelName":"src/utils/getPanelName.js","./utils/OneDToND":"src/utils/OneDToND.js"}],"node_modules/_parcel-bundler@1.12.5@parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "2286" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["node_modules/_parcel-bundler@1.12.5@parcel-bundler/src/builtins/hmr-runtime.js","src/createPanel.js"], null)
//# sourceMappingURL=/createPanel.1f4df4f2.js.map