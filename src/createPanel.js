import result from "../assets/result";
import getNames from "./utils/getPanelName";
import oneToN from "./utils/OneDToND";

//ease function

function ease(t, b, c, d) {
  t /= d / 2;
  if (t < 1) return (c / 2) * t * t * t * t + b;
  t -= 2;
  return (-c / 2) * (t * t * t * t - 2) + b;
}

//  2个页面
const container = document.querySelector(".container");
const underlying = document.querySelector(".underlying");

//  增加bigColorPanel 点击返回的功能
// 类名 (back-to-future)

const backToFuture = document.querySelector(".back-to-future");

backToFuture.addEventListener("click", () => {
  underlying.classList.remove("show");
  container.classList.remove("hide");
  let tmp = underlying.querySelector(".big-container");
  tmp.remove();
  document.querySelector("body").classList.remove("hide");
});

// ! 因为只有14个色板，所以索引是0-13 ;
const currentPanelIndex = {
  idx: 0,
  set currentIdx(num) {
    if (num < 0 || num > 13) {
      console.log("无法修改，超过索引");
      return;
    }
    this.idx = num;
    // console.log("修改成功", this);

    return this.idx;
  },
  get currentIdx() {
    return this.idx;
  },
  liveIdx: () => {
    return this.idx;
  },
};

//  原始的flat ui 上 一共十四个色盘 这里创建14个色盘
//  其实更好的 办法是固定写好在HTML里 ，但是我怕以后要增加色盘个数，所以写个能根据要求生成色盘的函数

// 创建14个 下面的结构

/**
    <div class="panel">
          <div class="colors">
            <div class="color"></div> 要20个
          </div>
          <div class="caption">
          </div>
        </div>
  */

//  配置就是 几个设定好的类名 ，和CSS 样式耦合 ，要改一起改。
const config = {
  panelClassName: "panel",
  colorsClassName: "colors",
  colorClassName: "color",
  captionClassName: "captions",
};
// 这是个辅助函数 把result里的的 name 以数组形式返回
const names = getNames(result);
// 接受类名作为参数
function createDiv(className) {
  const div = document.createElement("div");
  div.classList.add(className);
  return div;
}

// 接受参数 name 作为这个panel的标题 。
// 接受参数 idxOfResult, 选择result中第idx个对象
// 返回1个 div.panel 这个元素 有一个div.color ;div.color内部有2个div.colors
// 还有一个div.caption
function createPanel(name, idxOfResult) {
  const { panelClassName, colorsClassName, colorClassName, captionClassName } =
    config;
  const currentPanel = createDiv(panelClassName);
  // !给 div.panel绑定事件监听处理函数
  currentPanel.addEventListener("mousedown", (e) => {
    // 让 色盘隐藏 ；这里body是让他背景色变成白色，没有也行
    setTimeout(() => {
      container.classList.add("hide");
      document.querySelector("body").classList.add("hide");
      document.querySelector("div.mr-white").classList.add("animateRight");
      // window.requestAnimationFrame(moveLeft);
    }, 300);
    // 点击出现 一个遮罩左移的动画
    underlying.classList.add("show");
    let start = null;
    let element = document.getElementsByClassName("mask")[0];
    //
    function moveLeft(timestamp) {
      if (!start) {
        start = timestamp;
        0;
        element.classList.add("show");
      }
      let progress = timestamp - start;
      // element.style.right = Math.min(progress / 10, 200) + 'px';
      // const run = ease(progress, sPostion, tPosition as number, duration);
      const left = ease(progress, 200, -200, 1000);
      const width = ease(progress, 200, -200, 1000);
      element.style.left = left + "px";
      element.style.width = width + "vw";

      if (progress < 1000) {
        window.requestAnimationFrame(moveLeft);
      }
    }

    // ! 使用全局变量 currentPanelIndex={idx:0} 默认idx是0
    currentPanelIndex.currentIdx = idxOfResult;
    const panelBeingClicked = e.currentTarget;

    const underlyingEle = new CreateBigPanel(idxOfResult);
    //  这是一个点击缩小又还原的动画
    panelBeingClicked.classList.add("panel-sm");
    setTimeout(() => {
      panelBeingClicked.classList.remove("panel-sm");
    }, 120);

    // console.log(currentPanelIndex);
  });

  const colors = createDiv(colorsClassName);
  // 选取result里第idx个 对象
  // console.log(idxOfResult);
  const currentObjOfResult = result[idxOfResult];
  // 这个对象对应的数组
  const currentKey = Object.keys(currentObjOfResult);
  const constRGBArr = currentObjOfResult[currentKey];
  // 一个色块colors中有2个color
  const colorsNodeList = Array(20)
    .fill("emptyString")
    .map((itemThatIWontUse, idx) => {
      const currentColor = createDiv(colorClassName);

      currentColor.style.backgroundColor = constRGBArr[idx];
      return currentColor;
    });

  Array(20)
    .fill("why dont u use loop?dick head!")
    .forEach((itemThatIWontUse, idx) => {
      colors.appendChild(colorsNodeList[idx]);
    });
  // 用panel组织 colors>.color*20+caption这个结构
  currentPanel.appendChild(colors);
  // caption 名字在这里添加吧
  const currentCaption = createDiv(captionClassName);
  currentCaption.innerHTML = name;
  currentPanel.appendChild(currentCaption);

  // 测试通过了
  // console.log(currentPanel);

  return currentPanel;
}
// 测试通过了
// console.log(createPanel());

// 创建14个div.panel
const panels = Array(14)
  .fill("i shoud use loop")
  .map((placeholder, idx) => createPanel(names[idx], idx));

// 测试通过了
// console.log(panels);

// const container = document.querySelector(".container");

Array(14)
  .fill("i shoud use loop")
  .forEach((item, idx) => {
    container.appendChild(panels[idx]);
  });

// 第二个页面

const configOfBigPanel = {
  bigContainer: "big-container",
  bigCaption: "big-caption",
  bigColors: "big-colors",
  row: "big-row",
  bigColor: "big-color",
};
class CreateBigPanel {
  // 构造函数 创建 一个div.big-container
  // 创建一个div.big-caption
  // 创建一个div.big-colors
  // 创建20个div.big-color
  /**
       结构:
       div.big-container
          -div.big-caption
          -div.big-colors
              -div.bing-color*20
       */

  constructor(idx) {
    this.currentIdx = idx;
    this.bigContainer = CreateBigPanel.createDiv(configOfBigPanel.bigContainer);
    this.bigCaption = CreateBigPanel.createDiv(configOfBigPanel.bigCaption);

    this.bigColors = CreateBigPanel.createDiv(configOfBigPanel.bigColors);
    // 是一个数组 五行 div.big-row
    this.bigRow = CreateBigPanel.createDivs(configOfBigPanel.row, 5);
    // 是一个数组 ，不要了
    // this.bigColorArr = CreateBigPanel.createDivs(
    //   configOfBigPanel.bigColors,
    //   20
    // );
    this.flashGone = document.querySelector(".flash-gone");
    // 组合 辅助函数
    this._combine();
    // 渲染到页面上
    document.querySelector(".underlying").appendChild(this.bigContainer);
  }

  // 辅助函数 创建div.className
  static createDiv(className) {
    const div = document.createElement("div");
    div.classList.add(className);
    return div;
  }

  // 辅助函数 创建nums个div.className 返回一个数组
  static createDivs(className, nums) {
    let i = 0;
    const divArr = [];
    while (i < nums) {
      const div = document.createElement("div");
      div.classList.add(className);
      divArr.push(div);
      i++;
    }
    return divArr;
  }

  /**
  https://stackoverflow.com/questions/33855641/copy-output-of-a-javascript-variable-to-the-clipboard
  copied from a post talking about copy!
  _(:з」∠)_
   */
  static createInputForCopy(val) {
    let dummy = document.createElement("input");
    // !chrome  兼容问题？ 这一行不被采纳?
    // dummy.style.display = "none";
    document.body.appendChild(dummy);

    dummy.setAttribute("id", "dummy_id");
    document.getElementById("dummy_id").value = val;

    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
  }

  // 组合 实例属性 形成要求的结构
  _combine() {
    const currentPanelObj = result[this.currentIdx];
    const currentPanelName = Object.keys(currentPanelObj)[0];
    const currentPanelArr = currentPanelObj[currentPanelName];
    //! console.log(currentPanelArr); 测试通过 这是 当前要用到的色块的数组

    // 把5个小色块加到4行big-row中 一行+五个，所以一共20个
    // ! 把 1D数组 result 变成4*5数组  数组在这变了 ！ 要改在这改 （比如6行8列）
    const transformedArr = oneToN(4, 5, currentPanelArr);
    console.log(transformedArr);

    const bigRows = this.bigRow.map((row, ithRow) => {
      // 一行+五个
      for (let i = 0; i < 5; i++) {
        if (ithRow < 4) {
          const currentColor = transformedArr[ithRow][i];
          const currentDiv = CreateBigPanel.createDiv(
            configOfBigPanel.bigColor
          );
          const copyButton = CreateBigPanel.createDiv("hover-show");
          copyButton.innerText = "COPY";
          copyButton.addEventListener("mousemove", (e) => {
            e.currentTarget.classList.add("show");
          });
          copyButton.addEventListener("mouseleave", (e) => {
            e.currentTarget.classList.remove("show");
          });
          currentDiv.append(copyButton);
          currentDiv.style.backgroundColor = currentColor;
          // currentDiv.innerHTML=currentColor
          currentDiv.addEventListener("click", (e) => {
            const colorToCopy = e.currentTarget.style.backgroundColor;
            //!复制粘贴操作!
            CreateBigPanel.createInputForCopy(colorToCopy);

            // 显示COPY的动画
            this.flashGone.classList.add("show");
            this.flashGone.style.backgroundColor = colorToCopy;

            setTimeout(() => {
              this.flashGone.classList.remove("show");
            }, 300);
          });
          //  把 big-color小色块加到每一行里边
          row.append(currentDiv);
        }
      }
      return row;
    });

    // 把4行 big-row加到 big-colors
    Array(4)
      .fill("_")
      .forEach((_, idx) => {
        this.bigColors.append(bigRows[idx]);
      });
    // this.bigColors.append(bigRows);
    // console.log(bigRows);

    this.bigContainer.append(this.bigCaption);
    this.bigContainer.append(this.bigColors);

    // 找到当前页面的 20个 big-color 色块
    // const underlying= document.querySelectorAll(".big-color");
    //   console.log(underlying);

    // return this._combine
  }
  //  插入到页面.mask中

  // 给这个对象增加返回
}

//
