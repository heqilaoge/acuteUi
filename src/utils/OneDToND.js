

export default function oneToN(row, col, arr) {
  // 一维数组 这么复制就算深复制了
  const copyArr = [...arr];
  // 几行几列？ row行 col列
  const len = copyArr.length;
  if (len !== row * col) {
    throw new Error("做不了，个数不对");
  }
  const tempArr = [];
  for (let i = 0; i < row; i++) {
    //
    if (!tempArr[i]) {
      tempArr[i] = [];
    }
    let j = 0;
    while (j < col) {
      tempArr[i].push(copyArr.shift());
      j++;
    }
  }
//   console.log(tempArr);
  return tempArr;
}


