// 返回一个数组 ，包含了每个panel的标题
export default function getName(result) {
  const names = result.map((panelObj, idx) => {
    const currentName = Object.keys(panelObj)[0];
    return currentName;
  });
  return names;
}
